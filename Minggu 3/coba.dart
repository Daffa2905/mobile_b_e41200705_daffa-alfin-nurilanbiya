import 'dart:io';
void main(List<String> args) {
  hello();
  print("------------------------------------");
  hello2("Roger", 14);
}
void hello() {
  print("Hello, Roger");
}
void hello2(String nama, int hari) {
  print("Hallo, $nama");
  print("Anda sedang presentasi di pertemuan ke $hari");
}

// Arrow function
void hello3(String nama, int hari) => // '=>' mendifinisikan return
  "\n Anda sedang presentasi di pertemuan ke $hari";