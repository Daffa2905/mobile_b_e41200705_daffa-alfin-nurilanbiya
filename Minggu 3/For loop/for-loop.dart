void main(List<String> args) {
  // for(var a = 1; a < 10; a++) {
  // print (a); // Merupakan proses yang akan dijalankan dalam satu iterasi
  // }

  // // Looping For-loop 1 Looping Angka 1-9 Sederhana
  // for(var angka = 1; angka < 10; angka++) {
  // print('Iterasi ke-' + angka.toString());
  // }

  // // For-loop 2 Looping Mengembalikan Angka Total
  // var jumlah = 0;
  // for (var deret = 5; deret > 0; deret--) {
  // jumlah += deret;
  // print('Jumlah saat ini: ' + jumlah.toString());
  // }
  // print('Jumlah terakhir: ' + jumlah.toString());

  // Looping For-loop 3 Looping Dengan Increment dan Decrement Lebih dari 1
  for (var deret = 0; deret < 10; deret += 2) {
  print('Iterasi dengan Increment counter 2: ' + deret.toString());
  }
  print('-------------------------------');
  for (var deret = 15; deret > 0; deret -= 3) {
  print('Iterasi dengan Decrement counter : ' + deret.toString());
  }
}