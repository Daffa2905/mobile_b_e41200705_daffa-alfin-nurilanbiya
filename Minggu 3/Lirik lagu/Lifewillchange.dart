import 'dart:async';

void main() {
  print("Persona 5 - Life will change");
  print("----------------------------");

  Timer(Duration(seconds: 3), () => print("So we know that we're out there"));
  Timer(Duration(seconds: 6), () => print("Swatting lies in the making"));
  Timer(
      Duration(seconds: 9), () => print("Can't move fast without breaking"));
  Timer(Duration(seconds: 12), () => print("Can't hold on or life won't change"));
  Timer(Duration(seconds: 15), () => print("And our voices ring out, yeah"));
  Timer(Duration(seconds: 18), () => print("Took the mask off to feel free"));
}
