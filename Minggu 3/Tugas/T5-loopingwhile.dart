void main(List<String> args) {
  var urut = 2;
  print("LOOPING PERTAMA");
  while (urut <= 20) {
    print("$urut -  I love coding");
    urut += 2;
  }
  print("LOOPING KEDUA");
  var angka = 20;
  var jumlah = 0;
  while (angka > 0) {
    print("$angka - I will become a mobile developer");
    jumlah +=angka;
    angka -= 2;
  }
}