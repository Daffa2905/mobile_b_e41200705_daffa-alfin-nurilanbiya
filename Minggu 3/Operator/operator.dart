void main () {

  // // OPERATOR ASSIGNMENT

  // var angka;
  // angka = 10; // Contoh assignment variable angka dengan nilai 10

  // OPERATOR PERBANDINGAN

  // equal operator (==)
  // var angka = 100;
  // print(angka == 100); // true
  // print(angka == 20); // false

  // //  Not Equal ( != )
  // var sifat = "rajin";
  // print(sifat != "malas"); // true
  // print(sifat != "bandel"); //true
  
  // // Strict Equal ( === )
  // var angka = 8;
  // print(angka == "8"); // true, padahal "8" adalah string.
  // print(angka === "8"); // false, karena tipe data nya berbeda
  // print(angka === 8); // true

  // var number = 17;
  // print( number < 20 ); // true
  // print( number > 17 ); // false
  // print( number >= 17 ); // true, karena terdapat sama dengan
  // print( number <= 20 ); // true

  // //  OR ( || )
  // print(true || true); // true
  // print(true || false); // true
  // print(true || false || false); // true
  // print(false || false); // false

  // //  AND ( && )
  // print(true && true); // true
  // print(true && false); // false
  // print(false && false); // false
  // print(false && true && true); // false
  // print(true && true && true); // true
}
